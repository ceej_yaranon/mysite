from django.contrib import admin

# Register your models here.
from .models import Author, Book, Publisher

class AuthorAdmin(admin.ModelAdmin):
    fields = ['email', 'first_name', 'last_name']

class BookAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'authors', 'publisher']}),
        ('Date information', {
            'fields': ['publication_date'],
        }),
    ]

    list_display = ['id', 'title', 'publisher']

class BookInline(admin.TabularInline):
    model = Book
    extra = 1

class PublisherAdmin(admin.ModelAdmin):
    inlines = [BookInline]

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Publisher, PublisherAdmin)
