import datetime

from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template

# Create your views here.
def hello(request):
	return HttpResponse("Hello World")

def current_datetime(request):
  now = datetime.datetime.now()
  context = {'current_date': now}
  return render(
  	request,
  	'current_datetime.html',
  	context
  )

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    context = {'next_time':dt, 'hour_offset':offset}
    return render(
    	request,
    	'hours_ahead.html',
    	context
    )

def welcome(request):
	return HttpResponse("Welcome to Math!")

def add_any(request, num1, num2):
	try:
		num1 = int(num1)
		num2 = int(num2)
	except ValueError:
		raise Http404()
	result = num1 + num2
	html = "<html><body>The sum of %s and %s is %s.</body></html>" %	(num1, num2, result)
	return HttpResponse(html)

def sub_any(request, num1, num2):
	try:
		num1 = int(num1)
		num2 = int(num2)
	except ValueError:
		raise Http404()
	result = num1 - num2
	html = "<html><body>The difference of %s and %s is %s.</body></html>" %	(num1, num2, result)
	return HttpResponse(html)

def product_any(request, num1, num2):
	try:
		num1 = int(num1)
		num2 = int(num2)
	except ValueError:
		raise Http404()
	result = num1 * num2
	html = "<html><body>The product of %s and %s is %s.</body></html>" % (num1, num2, result)
	return HttpResponse(html)

def square_any(request, num1):
	try:
		num1 = int(num1)
	except ValueError:
		raise Http404()
	result = num1 * num1
	html = "<html><body>The square of %s is %s. </body></html>" % (num1, result)
	return HttpResponse(html)